﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionBarrUpOut : Option
    {
        public double Ibarr { get; set; }
        public OptionBarrUpOut(double S, double K, double mu, double Barr, double T, double sigma, int trail, int N, bool isput, Path path)
        {
            Is = S;
            Ik = K;
            Imu = mu;
            Ibarr = Barr;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            Isput = isput;
            P = path;
        }

        public double[,] LastPrice()
        {
            //Calculate the price of a Barrier Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                double maxnum = p[i, 0];
                for (int j = 1; j < In; j++)
                {
                    maxnum = Math.Max(p[i, j], maxnum);
                }
                if (Isput == false) //Barrier Call option
                {
                    if (maxnum > Ibarr)
                    {
                        Last[i, 0] = 0;
                    }
                    else
                    {
                        Last[i, 0] = Math.Max(p[i, In - 1] - Ik, 0);
                    }//up and out
                }
                else //Barrier Put option
                {
                    if (maxnum > Ibarr)
                    {
                        Last[i, 0] = 0;
                    }
                    else
                    {
                        Last[i, 0] = Math.Max(Ik - p[i, In - 1], 0);
                    }//up and out
                }
            }
            return Last;
        }

        public double PriceBarrUpOut(out double SE)
        {
            //Calculate the option price
            double[,] Last_Price;
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionBarrUpOut opb = new OptionBarrUpOut(1.001 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrUpOut ops = new OptionBarrUpOut(0.999 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrUpOut(out double SEb) - ops.PriceBarrUpOut(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionBarrUpOut opb = new OptionBarrUpOut(1.001 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrUpOut ops = new OptionBarrUpOut(0.999 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrUpOut(out double SEb) - 2 * PriceBarrUpOut(out double SE) + ops.PriceBarrUpOut(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionBarrUpOut opb = new OptionBarrUpOut(Is, Ik, Imu, Ibarr, It, 0.999 * Isigma, Itrail, In, Isput, P);
            OptionBarrUpOut ops = new OptionBarrUpOut(Is, Ik, Imu, Ibarr, It, 1.001 * Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrUpOut(out double SEb) - ops.PriceBarrUpOut(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionBarrUpOut opb = new OptionBarrUpOut(Is, Ik, Imu, Ibarr, 1.001 * It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrUpOut(out double SEb) - PriceBarrUpOut(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionBarrUpOut opb = new OptionBarrUpOut(Is, Ik, 0.999 * Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrUpOut ops = new OptionBarrUpOut(Is, Ik, 1.001 * Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrUpOut(out double SEb) - ops.PriceBarrUpOut(out double SEs)) / (0.002 * Imu);
        }
    }
}
