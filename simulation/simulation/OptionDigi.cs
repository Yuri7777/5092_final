﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionDigi : Option
    {
        public double Irebate { get; set; }

        public OptionDigi(double S, double K, double mu, double Rebate, double T, double sigma, int trail, int N, bool isput, Path path)
        {
            Is = S;
            Ik = K;
            Irebate = Rebate;
            Imu = mu;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            P = path;
            Isput = isput;
        }

        public double[,] LastPrice()
        {
            //Calculate the price of a Digital Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                if (Isput == false) //Digital Call option
                {
                    if (p[i, In - 1] >= Ik)
                    {
                        Last[i, 0] = Irebate;
                    }
                    else
                    {
                        Last[i, 0] = 0;
                    }
                }
                else //Digital Put option
                {
                    if (p[i, In - 1] <= Ik)
                    {
                        Last[i, 0] = Irebate;
                    }
                    else
                    {
                        Last[i, 0] = 0;
                    }
                }
            }
            return Last;
        }

        public double PriceDigi(out double SE)
        {
            //Calculate the option price
            double[,] Last_Price;
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionDigi opb = new OptionDigi(1.001 * Is, Ik, Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            OptionDigi ops = new OptionDigi(0.999 * Is, Ik, Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceDigi(out double SEb) - ops.PriceDigi(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionDigi opb = new OptionDigi(1.001 * Is, Ik, Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            OptionDigi ops = new OptionDigi(0.999 * Is, Ik, Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceDigi(out double SEb) - 2 * PriceDigi(out double SE) + ops.PriceDigi(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionDigi opb = new OptionDigi(Is, Ik, Imu, Irebate, It, 1.001 * Isigma, Itrail, In, Isput, P);
            OptionDigi ops = new OptionDigi(Is, Ik, Imu, Irebate, It, 0.999 * Isigma, Itrail, In, Isput, P);
            return (opb.PriceDigi(out double SEb) - ops.PriceDigi(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionDigi opb = new OptionDigi(Is, Ik, Imu, Irebate, 1.001 * It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceDigi(out double SEb) - PriceDigi(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionDigi opb = new OptionDigi(Is, Ik, 1.001 * Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            OptionDigi ops = new OptionDigi(Is, Ik, 0.999 * Imu, Irebate, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceDigi(out double SEb) - ops.PriceDigi(out double SEs)) / (0.002 * Imu);
        }
    }
}