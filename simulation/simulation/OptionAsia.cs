﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionAsia : Option
    {
        public OptionAsia(double S, double K, double mu, double T, double sigma, int trail, int N, bool isput, Path path)
        {
            Is = S;
            Ik = K;
            Imu = mu;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            P = path;
            Isput = isput;
        }
        public double[,] LastPrice()
        {
            //Calculate the price of an Asian Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                double sum = 0;
                for (int j = 0; j < In; j++)
                {
                    sum = sum + p[i, j];
                }
                if (Isput == false) //Asian Call option
                {
                    Last[i, 0] = Math.Max(sum / In - Ik, 0);
                }
                else //Asian Put option
                {
                    Last[i, 0] = Math.Max(Ik - sum / In, 0);
                }
            }
            return Last;
        }

        public double PriceAsia(out double SE)
        {
            //Calculate the option price
            double[,] Last_Price;
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionAsia opb = new OptionAsia(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            OptionAsia ops = new OptionAsia(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceAsia(out double SEb) - ops.PriceAsia(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionAsia opb = new OptionAsia(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            OptionAsia ops = new OptionAsia(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceAsia(out double SEb) - 2 * PriceAsia(out double SE) + ops.PriceAsia(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionAsia opb = new OptionAsia(Is, Ik, Imu, It, 1.001 * Isigma, Itrail, In, Isput, P);
            OptionAsia ops = new OptionAsia(Is, Ik, Imu, It, 0.999 * Isigma, Itrail, In, Isput, P);
            return (opb.PriceAsia(out double SEb) - ops.PriceAsia(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionAsia opb = new OptionAsia(Is, Ik, Imu, 1.001 * It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceAsia(out double SEb) - PriceAsia(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionAsia opb = new OptionAsia(Is, Ik, 1.001 * Imu, It, Isigma, Itrail, In, Isput, P);
            OptionAsia ops = new OptionAsia(Is, Ik, 0.999 * Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceAsia(out double SEb) - ops.PriceAsia(out double SEs)) / (0.002 * Imu);
        }
    }
}
