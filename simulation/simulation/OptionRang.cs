﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionRang : Option
    {
        public OptionRang(double S, double K, double mu, double T, double sigma, int trail, int N, Path path)
        {
            Is = S;
            Ik = K;
            Imu = mu;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            P = path;
        }

        public double[,] LastPrice()
        {
            //Calculate the price of a Range Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                double minnum = p[i, 0];
                double maxnum = p[i, 0];
                for (int j = 1; j < In; j++)
                {
                    minnum = Math.Min(p[i, j], minnum);
                    maxnum = Math.Max(p[i, j], maxnum);
                }
                Last[i, 0] = maxnum - minnum;
            }
            return Last;
        }
        public double PriceRang(out double SE)
        {
            double[,] Last_Price;
            //Calculate the option price
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionRang opb = new OptionRang(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, P);
            OptionRang ops = new OptionRang(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, P);
            return (opb.PriceRang(out double SEb) - ops.PriceRang(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionRang opb = new OptionRang(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, P);
            OptionRang ops = new OptionRang(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, P);
            return (opb.PriceRang(out double SEb) - 2 * PriceRang(out double SE) + ops.PriceRang(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionRang opb = new OptionRang(Is, Ik, Imu, It, 1.001 * Isigma, Itrail, In, P);
            OptionRang ops = new OptionRang(Is, Ik, Imu, It, 0.999 * Isigma, Itrail, In, P);
            return (opb.PriceRang(out double SEb) - ops.PriceRang(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionRang opb = new OptionRang(Is, Ik, Imu, 1.001 * It, Isigma, Itrail, In, P);
            return (opb.PriceRang(out double SEb) - PriceRang(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionRang opb = new OptionRang(Is, Ik, 1.001 * Imu, It, Isigma, Itrail, In, P);
            OptionRang ops = new OptionRang(Is, Ik, 0.999 * Imu, It, Isigma, Itrail, In, P);
            return (opb.PriceRang(out double SEb) - ops.PriceRang(out double SEs)) / (0.002 * Imu);
        }
    }
}
