﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class Option
    {
        //Set the input of an option.
        public Path P { get; set; }
        public double Is { get; set; }
        public double Ik { get; set; }
        public double Imu { get; set; }
        public double It { get; set; }
        public double Isigma { get; set; }
        public int Itrail { get; set; }
        public int In { get; set; }
        public bool Isput { get; set; }

        //Set the delta function of an option.
        public virtual double GreekDelta() { return 0; }
        public virtual double GreekGamma() { return 0; }
        public virtual double GreekVega() { return 0; }
        public virtual double GreekTheta() { return 0; }
        public virtual double GreekRho() { return 0; }
    }
}
