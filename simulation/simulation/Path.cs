﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class Path
    {
        double[,] Num { get; set; }
        public Path(int trail, int N)
        {
            Num = RandomNumber.GetRandom(trail, N);
        }

        public double[,] GetPath(double S, double mu, double T, double sigma, int trail, int N)
        {
            //Plot the MonteCarlo path. trail: the number of path, N: the step of each paths.
            double[,] undx = new double[trail, N];
            double dt = T / N;
            for (int i = 0; i < trail; i++)
            {
                undx[i, 0] = S;
                for (int j = 1; j < N; j++)
                {
                    //The underlying asset price follows geometric brownian motion.
                    undx[i, j] = undx[i, j - 1] * Math.Exp((mu - 0.5 * sigma * sigma) * dt + sigma * Math.Sqrt(dt) * Num[i, j - 1]);
                }
            }
            return undx;
        }
    }
}
