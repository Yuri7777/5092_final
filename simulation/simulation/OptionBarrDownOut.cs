﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionBarrDownOut : Option
    {
        public double Ibarr { get; set; }
        public OptionBarrDownOut(double S, double K, double mu, double Barr, double T, double sigma, int trail, int N, bool isput, Path path)
        {
            Is = S;
            Ik = K;
            Imu = mu;
            Ibarr = Barr;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            Isput = isput;
            P = path;
        }

        public double[,] LastPrice()
        {
            //Calculate the price of a Barrier Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                double minnum = p[i, 0];
                for (int j = 1; j < In; j++)
                {
                    minnum = Math.Min(p[i, j], minnum);
                }
                if (Isput == false) //Barrier Call option
                {
                    if (minnum < Ibarr)
                    {
                        Last[i, 0] = 0;
                    }
                    else
                    {
                        Last[i, 0] = Math.Max(p[i, In - 1] - Ik, 0);
                    }//down and out
                }
                else //Barrier Put option
                {
                    if (minnum < Ibarr)
                    {
                        Last[i, 0] = 0;
                    }
                    else
                    {
                        Last[i, 0] = Math.Max(Ik - p[i, In - 1], 0);
                    }//down and out
                }
            }
            return Last;
        }

        public double PriceBarrDownOut(out double SE)
        {
            //Calculate the option price
            double[,] Last_Price;
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionBarrDownOut opb = new OptionBarrDownOut(1.001 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrDownOut ops = new OptionBarrDownOut(0.999 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrDownOut(out double SEb) - ops.PriceBarrDownOut(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionBarrDownOut opb = new OptionBarrDownOut(1.001 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrDownOut ops = new OptionBarrDownOut(0.999 * Is, Ik, Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrDownOut(out double SEb) - 2 * PriceBarrDownOut(out double SE) + ops.PriceBarrDownOut(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionBarrDownOut opb = new OptionBarrDownOut(Is, Ik, Imu, Ibarr, It, 0.999 * Isigma, Itrail, In, Isput, P);
            OptionBarrDownOut ops = new OptionBarrDownOut(Is, Ik, Imu, Ibarr, It, 1.001 * Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrDownOut(out double SEb) - ops.PriceBarrDownOut(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionBarrDownOut opb = new OptionBarrDownOut(Is, Ik, Imu, Ibarr, 1.001 * It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrDownOut(out double SEb) - PriceBarrDownOut(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionBarrDownOut opb = new OptionBarrDownOut(Is, Ik, 0.999 * Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            OptionBarrDownOut ops = new OptionBarrDownOut(Is, Ik, 1.001 * Imu, Ibarr, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceBarrDownOut(out double SEb) - ops.PriceBarrDownOut(out double SEs)) / (0.002 * Imu);
        }
    }
}