﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    static class RandomNumber
    {
        private static Random rnd = new Random();

        public static double[,] GetRandom(int trail, int N) //Generate random variables following standard normal distribution.
        {
            double[,] Random = new double[trail, N]; //Pollar rejection method.
            for (int i = 0; i < trail; i++)
            {
                for (int j = 0; j < N / 2; j++)
                {
                    double x1, x2, w;
                    do
                    {
                        Random r1 = rnd;
                        x1 = 2 * r1.NextDouble() - 1;
                        Random r2 = rnd;
                        x2 = 2 * r2.NextDouble() - 1;
                        w = x1 * x1 + x2 * x2;
                    } while (w > 1);
                    Random[i, j] = x1 * Math.Sqrt(-2 * Math.Log(w) / w);
                    Random[i, j + N / 2] = x2 * Math.Sqrt(-2 * Math.Log(w) / w);
                }
            }
            return Random;
        }
    }
}
