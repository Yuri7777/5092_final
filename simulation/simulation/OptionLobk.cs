﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class OptionLobk : Option
    {
        public OptionLobk(double S, double K, double mu, double T, double sigma, int trail, int N, bool isput, Path path)
        {
            Is = S;
            Ik = K;
            Imu = mu;
            It = T;
            Isigma = sigma;
            Itrail = trail;
            In = N;
            P = path;
            Isput = isput;
        }

        public double[,] LastPrice()
        {
            //Calculate the price of a Lookback Option on the final step and discount to current moment.
            double[,] p = P.GetPath(Is, Imu, It, Isigma, Itrail, In);
            double[,] Last = new double[Itrail, 1];
            for (int i = 0; i < Itrail; i++)
            {
                if (Isput == false) //Lookback Call option
                {
                    double maxnum = p[i, 0];
                    for (int j = 1; j < In; j++)
                    {
                        maxnum = Math.Max(p[i, j], maxnum);
                    }
                    Last[i, 0] = Math.Max(maxnum - Ik, 0);
                }
                else //Lookback Put option
                {
                    double minnum = p[i, 0];
                    for (int j = 1; j < In; j++)
                    {
                        minnum = Math.Min(p[i, j], minnum);
                    }
                    Last[i, 0] = Math.Max(Ik - minnum, 0);
                }
            }
            return Last;
        }
        public double PriceLobk(out double SE)
        {
            //Calculate the option price
            double[,] Last_Price;
            Last_Price = LastPrice();
            double cumsum = 0;
            double cumsum2 = 0;
            for (int i = 0; i < Itrail; i++)
            {
                cumsum = cumsum + Last_Price[i, 0];
                cumsum2 = cumsum2 + Last_Price[i, 0] * Last_Price[i, 0];
            }
            SE = Math.Sqrt((cumsum2 - cumsum * cumsum / Itrail) * Math.Exp(-2 * Imu * It) / (Itrail - 1)) / Math.Sqrt(Itrail);
            return Math.Exp(-Imu * It) * cumsum / Itrail;
        }

        public override double GreekDelta()
        {
            //Calculate Delta
            OptionLobk opb = new OptionLobk(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            OptionLobk ops = new OptionLobk(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceLobk(out double SEb) - ops.PriceLobk(out double SEs)) / (0.002 * Is);
        }

        public override double GreekGamma()
        {
            //Calculate Gamma
            OptionLobk opb = new OptionLobk(1.001 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            OptionLobk ops = new OptionLobk(0.999 * Is, Ik, Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceLobk(out double SEb) - 2 * PriceLobk(out double SE) + ops.PriceLobk(out double SEs)) / (0.001 * 0.001 * Is * Is);
        }

        public override double GreekVega()
        {
            //Calculate Vega
            OptionLobk opb = new OptionLobk(Is, Ik, Imu, It, 1.001 * Isigma, Itrail, In, Isput, P);
            OptionLobk ops = new OptionLobk(Is, Ik, Imu, It, 0.999 * Isigma, Itrail, In, Isput, P);
            return (opb.PriceLobk(out double SEb) - ops.PriceLobk(out double SEs)) / (0.002 * Isigma);
        }

        public override double GreekTheta()
        {
            //Calculate Theta
            OptionLobk opb = new OptionLobk(Is, Ik, Imu, 1.001 * It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceLobk(out double SEb) - PriceLobk(out double SEs)) / (-0.001 * It);
        }

        public override double GreekRho()
        {
            //Calculate Rho
            OptionLobk opb = new OptionLobk(Is, Ik, 1.001 * Imu, It, Isigma, Itrail, In, Isput, P);
            OptionLobk ops = new OptionLobk(Is, Ik, 0.999 * Imu, It, Isigma, Itrail, In, Isput, P);
            return (opb.PriceLobk(out double SEb) - ops.PriceLobk(out double SEs)) / (0.002 * Imu);
        }
    }
}