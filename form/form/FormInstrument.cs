﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormInstrument : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormInstrument()
        {
            InitializeComponent();
        }

        private void FormInstrument_Load(object sender, EventArgs e)
        {
            textBox6.Enabled = false;
            textBox8.Enabled = false;
            comboBox2.Items.AddRange((from i in fdbc.InstrTypeSet
                                      select i.TypeName).ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox8.Enabled = comboBox2.Text == "Digital Option";
            textBox6.Enabled = comboBox2.Text == "DownOut Barrier Option" || comboBox2.Text == "DownIn Barrier Option" || comboBox2.Text == "UpOut Barrier Option" || comboBox2.Text == "UpIn Barrier Option";
        }

        private void OK_Click(object sender, EventArgs e)
        {
            InstrType instrType = (from q in fdbc.InstrTypeSet
                                   where q.TypeName == comboBox2.Text
                                   select q).FirstOrDefault();

            fdbc.InstrumentSet.Add(new Instrument()
            {
                CompanyName = textBox1.Text,
                InstrTicker = textBox2.Text,
                UnderlyingTicker = textBox3.Text,
                UnderlyingPrice = Convert.ToDouble(textBox7.Text),
                Strike = Convert.ToDouble(textBox4.Text),
                Tenor = Convert.ToDouble(textBox5.Text),
                DigiRebate = textBox8.Enabled ? Convert.ToDouble(textBox8.Text) : 0,
                Barrier = textBox6.Enabled ? Convert.ToDouble(textBox6.Text) : 0,
                InstrTypeId = instrType.Id,
                Type = comboBox1.Text
            });
            fdbc.SaveChanges();
            MessageBox.Show("Instrument added successfully", "Confirmation", MessageBoxButtons.OK);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Please input the correct form of company name.");
                return;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                MessageBox.Show("Please input the correct form of ticker.");
                return;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Please input the correct form of underlying ticker.");
                return;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            double st;
            if (!double.TryParse(textBox4.Text, out st) || st <= 0)
            {
                textBox4.BackColor = Color.Pink;
                errorProvider1.SetError(textBox4, "Please input the correct form of strike price.");
                return;
            }
            else
            {
                textBox4.BackColor = Color.White;
                errorProvider1.SetError(textBox4, string.Empty);
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            double t;
            if (!double.TryParse(textBox4.Text, out t) || t <= 0)
            {
                textBox5.BackColor = Color.Pink;
                errorProvider2.SetError(textBox5, "Please input the correct form of tenor.");
                return;
            }
            else
            {
                textBox5.BackColor = Color.White;
                errorProvider2.SetError(textBox5, string.Empty);
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            double up;
            if (!double.TryParse(textBox7.Text, out up) || up <= 0)
            {
                textBox7.BackColor = Color.Pink;
                errorProvider5.SetError(textBox7, "Please input the correct form of underlying price.");
                return;
            }
            else
            {
                textBox7.BackColor = Color.White;
                errorProvider5.SetError(textBox7, string.Empty);
            }
        }
    }
}
