﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormInstType : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormInstType()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            fdbc.InstrTypeSet.Add(new InstrType()
            {
                TypeName = Convert.ToString(comboBox2.SelectedItem)
            });
            fdbc.SaveChanges();
            MessageBox.Show("Instrument type added successfully", "Confirmation", MessageBoxButtons.OK);
        }


    }
}
