﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormPriceAnalysis : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormPriceAnalysis()
        {
            InitializeComponent();
        }

        private void FormPriceAnalysis_Load(object sender, EventArgs e)
        {
            foreach (Instrument i in fdbc.InstrumentSet)
                comboBox2.Items.Add(i.UnderlyingTicker);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            var s = (from i in fdbc.PriceSet
                    where i.Ticker.ToUpper() == comboBox2.Text
                    select new { i.Date, i.ClosePrice }).ToArray();
            dataGridView1.DataSource = s;
        }
    }
}
