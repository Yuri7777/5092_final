﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;
using Simulator;

namespace TradeForm
{
    public partial class Form1 : Form
    {
        ListViewItem lv = null;
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public Form1()
        {
            InitializeComponent();
        }

        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInstType f = new FormInstType();
            f.ShowDialog();
        }

        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInstrument f = new FormInstrument();
            f.ShowDialog();
        }

        private void priceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPrice f = new FormPrice();
            f.ShowDialog();
        }

        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInterest f = new FormInterest();
            f.ShowDialog();
        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTrade f = new FormTrade();
            f.ShowDialog();
        }

        private void priceAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPriceAnalysis f = new FormPriceAnalysis();
            f.ShowDialog();
        }

        private void rateAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRateAnalysis f = new FormRateAnalysis();
            f.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void refreshDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            var t = (from q in fdbc.TradeSet
                     select q).FirstOrDefault();

            foreach(var trade in (from q in fdbc.TradeSet
                              select q))
            {
                lv = new ListViewItem(Convert.ToString(trade.Id));
                lv.SubItems.Add(Convert.ToString(trade.Quantity));
                lv.SubItems.Add(Convert.ToString(trade.IsBuy));
                lv.SubItems.Add(Convert.ToString(trade.Instrument.InstrTicker));
                lv.SubItems.Add(Convert.ToString(trade.Instrument.InstrType.TypeName));
                lv.SubItems.Add(Convert.ToString(trade.Delta));
                lv.SubItems.Add(Convert.ToString(trade.Gamma));
                lv.SubItems.Add(Convert.ToString(trade.Rho));
                lv.SubItems.Add(Convert.ToString(trade.Theta));
                lv.SubItems.Add(Convert.ToString(trade.Vega));
                lv.SubItems.Add(Convert.ToString(trade.PNL));
                lv.SubItems.Add(Convert.ToString(trade.MarketPrice));
                lv.SubItems.Add(Convert.ToString(trade.TradePrice));
                lv.SubItems.Add(Convert.ToString(trade.TradeTime));
                listView2.Items.Add(lv);
            }
        }

        private void calculatePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in listView2.Items)
            {
                int id = Convert.ToInt32(i.Text);
                Trade trade = (from q in fdbc.TradeSet
                               where q.Id == id
                               select q).FirstOrDefault();

                double rate;
                double ten = Convert.ToDouble(trade.Instrument.Tenor);

                if((from pi in fdbc.InterestRateSet
                    where pi.Tenor == ten
                    select pi.Rate).Count() == 0)
                {
                    double rateup = (from q in fdbc.InterestRateSet
                                     orderby q.Tenor descending
                                     where q.Tenor > trade.Instrument.Tenor
                                     select q.Rate).FirstOrDefault();
                    double tenorup = (from q in fdbc.InterestRateSet
                                     orderby q.Tenor descending
                                     where q.Tenor > trade.Instrument.Tenor
                                     select q.Tenor).FirstOrDefault();
                    double ratedown = (from q in fdbc.InterestRateSet
                                     orderby q.Tenor descending
                                     where q.Tenor < trade.Instrument.Tenor
                                     select q.Rate).FirstOrDefault();
                    double tenordown = (from q in fdbc.InterestRateSet
                                      orderby q.Tenor descending
                                      where q.Tenor < trade.Instrument.Tenor
                                      select q.Tenor).FirstOrDefault();
                    rate = ((rateup - ratedown) / (tenorup - tenordown)) * (ten - tenordown) + ratedown;
                }
                else
                {
                    rate = (from pi in fdbc.InterestRateSet
                             where pi.Tenor == ten
                             select pi.Rate).FirstOrDefault();
                }
                Path p = new Path(100, 252);
                switch (trade.Instrument.InstrType.TypeName)
                {
                    case "European Option":
                        double s = Convert.ToDouble(trade.OptionUnderlying);
                        double k = trade.Instrument.Strike;
                        double mu = rate;
                        double T = trade.Instrument.Tenor;
                        double sigma = Convert.ToDouble(textBox1.Text);
                        bool isput = trade.Instrument.Type == "Put" ? true : false;

                        double se;
                        OptionEuro oe = new OptionEuro(s, k, mu, T, sigma, 100, 252, isput, p);
                        double price = oe.PriceEuro(out se);
                        double delta = oe.GreekDelta();
                        double gamma = oe.GreekGamma();
                        double vega = oe.GreekVega();
                        double theta = oe.GreekTheta();
                        double rho = oe.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "Asian Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;

                        OptionAsia oa = new OptionAsia(s, k, mu, T, sigma, 100, 252, isput, p);
                        price = oa.PriceAsia(out se);
                        delta = oa.GreekDelta();
                        gamma = oa.GreekGamma();
                        vega = oa.GreekVega();
                        theta = oa.GreekTheta();
                        rho = oa.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "Digital Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        double rebate = Convert.ToDouble(trade.Instrument.DigiRebate);
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;

                        OptionDigi od = new OptionDigi(s, k, mu, rebate, T, sigma, 100, 252, isput, p);
                        price = od.PriceDigi(out se);
                        delta = od.GreekDelta();
                        gamma = od.GreekGamma();
                        vega = od.GreekVega();
                        theta = od.GreekTheta();
                        rho = od.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "Lookback Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;

                        OptionLobk ol = new OptionLobk(s, k, mu, T, sigma, 100, 252, isput, p);
                        price = ol.PriceLobk(out se);
                        delta = ol.GreekDelta();
                        gamma = ol.GreekGamma();
                        vega = ol.GreekVega();
                        theta = ol.GreekTheta();
                        rho = ol.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "Range Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);

                        OptionRang or = new OptionRang(s, k, mu, T, sigma, 100, 252, p);
                        price = or.PriceRang(out se);
                        delta = or.GreekDelta();
                        gamma = or.GreekGamma();
                        vega = or.GreekVega();
                        theta = or.GreekTheta();
                        rho = or.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "DownOut Barrier Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;
                        double barrier = Convert.ToDouble(trade.Instrument.Barrier);


                        OptionBarrDownOut obdo = new OptionBarrDownOut(s, k, mu, barrier, T, sigma, 100, 252, isput, p);
                        price = obdo.PriceBarrDownOut(out se);
                        delta = obdo.GreekDelta();
                        gamma = obdo.GreekGamma();
                        vega = obdo.GreekVega();
                        theta = obdo.GreekTheta();
                        rho = obdo.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "DownIn Barrier Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;
                        barrier = Convert.ToDouble(trade.Instrument.Barrier);


                        OptionBarrDownIn obdi = new OptionBarrDownIn(s, k, mu, barrier, T, sigma, 100, 252, isput, p);
                        price = obdi.PriceBarrDownIn(out se);
                        delta = obdi.GreekDelta();
                        gamma = obdi.GreekGamma();
                        vega = obdi.GreekVega();
                        theta = obdi.GreekTheta();
                        rho = obdi.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "UpOut Barrier Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;
                        barrier = Convert.ToDouble(trade.Instrument.Barrier);


                        OptionBarrUpOut obuo = new OptionBarrUpOut(s, k, mu, barrier, T, sigma, 100, 252, isput, p);
                        price = obuo.PriceBarrUpOut(out se);
                        delta = obuo.GreekDelta();
                        gamma = obuo.GreekGamma();
                        vega = obuo.GreekVega();
                        theta = obuo.GreekTheta();
                        rho = obuo.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "UpIn Barrier Option":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        k = trade.Instrument.Strike;
                        mu = rate;
                        T = trade.Instrument.Tenor;
                        sigma = Convert.ToDouble(textBox1.Text);
                        isput = trade.Instrument.Type == "Put" ? true : false;
                        barrier = Convert.ToDouble(trade.Instrument.Barrier);


                        OptionBarrUpIn obui = new OptionBarrUpIn(s, k, mu, barrier, T, sigma, 100, 252, isput, p);
                        price = obui.PriceBarrUpIn(out se);
                        delta = obui.GreekDelta();
                        gamma = obui.GreekGamma();
                        vega = obui.GreekVega();
                        theta = obui.GreekTheta();
                        rho = obui.GreekRho();
                        i.SubItems[11].Text = price.ToString();
                        i.SubItems[5].Text = (delta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[6].Text = (gamma * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[9].Text = (vega * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[8].Text = (theta * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[7].Text = (rho * (double)trade.Quantity * (trade.IsBuy == true ? 1 : -1) * (trade.Instrument.Type == "Call" ? 1 : -1)).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;

                    case "Stock":
                        s = Convert.ToDouble(trade.OptionUnderlying);
                        i.SubItems[11].Text = s.ToString();
                        i.SubItems[5].Text = trade.IsBuy == true ? trade.Quantity.ToString() : (-1 * trade.Quantity).ToString();
                        i.SubItems[10].Text = ((trade.IsBuy == true) ? ((Convert.ToDouble(i.SubItems[11].Text) - trade.TradePrice) * (double)trade.Quantity) : ((trade.TradePrice - Convert.ToDouble(i.SubItems[11].Text)) * (double)trade.Quantity)).ToString();

                        break;
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            double[] totalvalues = { 0, 0, 0, 0, 0, 0 };
            foreach (ListViewItem item in listView2.SelectedItems)
            {
                totalvalues[0] += Convert.ToDouble(item.SubItems[10].Text);
                totalvalues[1] += Convert.ToDouble(item.SubItems[5].Text);
                totalvalues[2] += Convert.ToDouble(item.SubItems[6].Text);
                totalvalues[3] += Convert.ToDouble(item.SubItems[9].Text);
                totalvalues[4] += Convert.ToDouble(item.SubItems[8].Text);
                totalvalues[5] += Convert.ToDouble(item.SubItems[7].Text);
            }

            string[] saved = new string[6];
            for (int i = 0; i < 6; i++)
            {
                saved[i] = totalvalues[i].ToString();
            }

            listView1.Items.Add(new ListViewItem(saved));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView2.Items.Remove(listView2.SelectedItems[0]);      
            
            int g = Convert.ToInt32(lv.SubItems[0].Text);
            fdbc.TradeSet.Remove((from i in fdbc.TradeSet
                               where i.Id == g
                               select i).FirstOrDefault());
            fdbc.SaveChanges();

            MessageBox.Show("Trade Deleted Successfully", "Confirmation", MessageBoxButtons.OK);
        }
    }
}
