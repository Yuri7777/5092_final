﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormRateAnalysis : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormRateAnalysis()
        {
            InitializeComponent();
        }

        private void FormRateAnalysis_Load(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            var q = (from i in fdbc.InterestRateSet
                    select new { i.Tenor, i.Rate }).ToArray();
            dataGridView1.DataSource = q;
        }
    }
}
