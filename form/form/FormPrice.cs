﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormPrice : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormPrice()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FormPrice_Load(object sender, EventArgs e)
        {
            foreach (Instrument i in fdbc.InstrumentSet)
                comboBox1.Items.Add(i.UnderlyingTicker);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fdbc.PriceSet.Add(new Price()
            {
                Ticker = Convert.ToString(comboBox1.SelectedItem),
                ClosePrice = Convert.ToDouble(textBox2.Text),
                Date = Convert.ToDateTime(dateTimePicker1.Value)
            });
            fdbc.SaveChanges();
            MessageBox.Show("Price added successfully", "Confirmation", MessageBoxButtons.OK);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            double cp;
            if (!double.TryParse(textBox2.Text, out cp) || cp <= 0)
            {
                textBox2.BackColor = Color.Pink;
                errorProvider1.SetError(textBox2, "Please input the correct form of ClosePrice.");
                return;
            }
            else
            {
                textBox2.BackColor = Color.White;
                errorProvider1.SetError(textBox2, string.Empty);
            }
        }
    }
}
