﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceDB;

namespace TradeForm
{
    public partial class FormTrade : Form
    {
        static FinanceDBContainer fdbc = new FinanceDBContainer();

        public FormTrade()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FormTrade_Load(object sender, EventArgs e)
        {
            comboBox2.Items.AddRange((from i in fdbc.InstrumentSet
                                      select i.InstrTicker).ToArray());
        }

        private void OK_Click(object sender, EventArgs e)
        {
            Instrument i = (from q in fdbc.InstrumentSet
                            where q.InstrTicker == comboBox2.Text
                            select q).FirstOrDefault();
            int instId = i.Id;

            var lp = from q in fdbc.PriceSet
                     where q.Ticker == i.UnderlyingTicker
                     select q;
            Price lastprice = lp.OrderByDescending(o => o.Date).FirstOrDefault();
            double underlyingprice = lastprice.ClosePrice;

            fdbc.TradeSet.Add(new Trade()
            {
                IsBuy = radioButton1.Checked,
                Quantity = Convert.ToInt64(textBox2.Text),
                TradePrice = Convert.ToDouble(textBox1.Text),
                MarketPrice = 0,
                TradeTime = dateTimePicker1.Value,
                OptionUnderlying = underlyingprice,
                PNL = 0,
                Delta = 0,
                Gamma = 0,
                Vega = 0,
                Theta = 0,
                Rho = 0,
                InstrumentId = instId,
            });
            fdbc.SaveChanges();
            MessageBox.Show("Trade added successfully.", "Confirmation", MessageBoxButtons.OK);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            double tp;
            if(!double.TryParse(textBox1.Text, out tp) || tp <= 0)
            {
                textBox1.BackColor = Color.Pink;
                errorProvider1.SetError(textBox1, "Please input the correct form of trade price.");
                return;
            }
            else
            {
                textBox1.BackColor = Color.White;
                errorProvider1.SetError(textBox1, string.Empty);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int quant;
            if (!int.TryParse(textBox1.Text, out quant) || quant <= 0)
            {
                textBox2.BackColor = Color.Pink;
                errorProvider2.SetError(textBox2, "Please input the correct form of trade price.");
                return;
            }
            else
            {
                textBox2.BackColor = Color.White;
                errorProvider2.SetError(textBox1, string.Empty);
            }
        }
    }
}
