//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinanceDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trade
    {
        public int Id { get; set; }
        public bool IsBuy { get; set; }
        public long Quantity { get; set; }
        public double TradePrice { get; set; }
        public double MarketPrice { get; set; }
        public System.DateTime TradeTime { get; set; }
        public Nullable<double> OptionUnderlying { get; set; }
        public double PNL { get; set; }
        public double Delta { get; set; }
        public Nullable<double> Gamma { get; set; }
        public Nullable<double> Vega { get; set; }
        public Nullable<double> Theta { get; set; }
        public Nullable<double> Rho { get; set; }
        public int InstrumentId { get; set; }
    
        public virtual Instrument Instrument { get; set; }
    }
}
