
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/09/2019 12:33:34
-- Generated from EDMX file: C:\Users\yifei\Documents\C# project\Final project\FinanceDB\FinanceDB\FinanceDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FinanceDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_InstrumentPrice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PriceSet] DROP CONSTRAINT [FK_InstrumentPrice];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrTypeInstrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InstrumentSet] DROP CONSTRAINT [FK_InstrTypeInstrument];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentTrade]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TradeSet] DROP CONSTRAINT [FK_InstrumentTrade];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[InstrumentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstrumentSet];
GO
IF OBJECT_ID(N'[dbo].[PriceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PriceSet];
GO
IF OBJECT_ID(N'[dbo].[InstrTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstrTypeSet];
GO
IF OBJECT_ID(N'[dbo].[InterestRateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InterestRateSet];
GO
IF OBJECT_ID(N'[dbo].[TradeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TradeSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'InstrumentSet'
CREATE TABLE [dbo].[InstrumentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [InstrTicker] nvarchar(max)  NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [DigiRebate] float  NULL,
    [Strike] float  NOT NULL,
    [Tenor] float  NOT NULL,
    [Barrier] float  NULL,
    [InstrTypeId] int  NOT NULL,
    [UnderlyingTicker] nvarchar(max)  NOT NULL,
    [UnderlyingPrice] float  NOT NULL
);
GO

-- Creating table 'PriceSet'
CREATE TABLE [dbo].[PriceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [ClosePrice] float  NOT NULL,
    [Ticker] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InstrTypeSet'
CREATE TABLE [dbo].[InstrTypeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InterestRateSet'
CREATE TABLE [dbo].[InterestRateSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tenor] float  NOT NULL,
    [Rate] float  NOT NULL
);
GO

-- Creating table 'TradeSet'
CREATE TABLE [dbo].[TradeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsBuy] bit  NOT NULL,
    [Quantity] bigint  NOT NULL,
    [TradePrice] float  NOT NULL,
    [MarketPrice] float  NOT NULL,
    [TradeTime] datetime  NOT NULL,
    [OptionUnderlying] float  NULL,
    [PNL] float  NOT NULL,
    [Delta] float  NOT NULL,
    [Gamma] float  NULL,
    [Vega] float  NULL,
    [Theta] float  NULL,
    [Rho] float  NULL,
    [InstrumentId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'InstrumentSet'
ALTER TABLE [dbo].[InstrumentSet]
ADD CONSTRAINT [PK_InstrumentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [PK_PriceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InstrTypeSet'
ALTER TABLE [dbo].[InstrTypeSet]
ADD CONSTRAINT [PK_InstrTypeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterestRateSet'
ALTER TABLE [dbo].[InterestRateSet]
ADD CONSTRAINT [PK_InterestRateSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TradeSet'
ALTER TABLE [dbo].[TradeSet]
ADD CONSTRAINT [PK_TradeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [InstrTypeId] in table 'InstrumentSet'
ALTER TABLE [dbo].[InstrumentSet]
ADD CONSTRAINT [FK_InstrTypeInstrument]
    FOREIGN KEY ([InstrTypeId])
    REFERENCES [dbo].[InstrTypeSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrTypeInstrument'
CREATE INDEX [IX_FK_InstrTypeInstrument]
ON [dbo].[InstrumentSet]
    ([InstrTypeId]);
GO

-- Creating foreign key on [InstrumentId] in table 'TradeSet'
ALTER TABLE [dbo].[TradeSet]
ADD CONSTRAINT [FK_InstrumentTrade]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[InstrumentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentTrade'
CREATE INDEX [IX_FK_InstrumentTrade]
ON [dbo].[TradeSet]
    ([InstrumentId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------