﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinanceDB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class FinanceDBContainer : DbContext
    {
        public FinanceDBContainer()
            : base("name=FinanceDBContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Instrument> InstrumentSet { get; set; }
        public virtual DbSet<Price> PriceSet { get; set; }
        public virtual DbSet<InstrType> InstrTypeSet { get; set; }
        public virtual DbSet<InterestRate> InterestRateSet { get; set; }
        public virtual DbSet<Trade> TradeSet { get; set; }
    }
}
